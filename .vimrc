set nocompatible
filetype off

" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

call plug#begin('~/.vim/plugged')

Plug 'zhou13/vim-easyescape'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'godlygeek/tabular'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'
Plug 'flazz/vim-colorschemes'
Plug 'sickill/vim-monokai'
Plug 'itchyny/lightline.vim'
Plug 'benmills/vimux'
Plug 'christoomey/vim-tmux-navigator'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'JamshedVesuna/vim-markdown-preview'
Plug 'tpope/vim-surround'
Plug 'psf/black'
Plug 'Yggdroot/indentLine'

Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Filetype plugins
Plug 'ekalinin/Dockerfile.vim'
Plug 'elzr/vim-json'
Plug 'pearofducks/ansible-vim'
Plug 'hashivim/vim-terraform'
Plug 'fatih/vim-go'
Plug 'VSpike/vim-jsonnet'
Plug 'gabrielelana/vim-markdown'
Plug 'pedrohdz/vim-yaml-folds'
Plug 'tarekbecker/vim-yaml-formatter'
Plug 'Joorem/vim-haproxy'
Plug 'itkq/fluentd-vim'

Plug 'w0rp/ale'

Plug 'ryanoasis/vim-devicons'

call plug#end()


set background=dark
if !has('gui_running')
  set t_Co=256
endif
colorscheme monokai

" Required for vim-devicons
set encoding=utf8

set backspace=indent,eol,start
set modeline
set modelines=5

set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
set softtabstop=4
set autoindent
set smartindent
set hlsearch
set cursorline
set lazyredraw
set smartcase
set ignorecase

set number
" set relativenumber

" Handle buffers better
set hidden
set confirm

" Make whitespace visible
set list listchars=tab:▷⋅,trail:⋅,nbsp:⋅

" Keymapping to replace word under cursor
nnoremap <Leader>s :%s/\<<C-r><C-w>\>//g<Left><Left>

" Easy Escape config
let g:easyescape_chars = { "j": 1, "k": 1 }
let g:easyescape_timeout = 2000
cnoremap jk <ESC>
cnoremap kj <ESC>

" vim-tmux-navigator config
let g:tmux_navigator_save_on_switch = 1

" Configure lightline
set laststatus=2

let g:lightline = {
      \ 'colorscheme': 'powerline',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'filetype': 'MyFiletype',
      \   'fileformat': 'MyFileformat',
      \   'gitbranch': 'fugitive#head',
      \ }
      \ }

function! MyFiletype()
  return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype . ' ' . WebDevIconsGetFileTypeSymbol() : 'no ft') : ''
endfunction

function! MyFileformat()
  return winwidth(0) > 70 ? (&fileformat . ' ' . WebDevIconsGetFileFormatSymbol()) : ''
endfunction

" Remap leader to space
let mapleader = "\<Space>"

" Edit and source vimrc file
nnoremap <leader>ev :vsplit $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>

" General useful mappings
nnoremap <leader><space> :nohlsearch<cr>
nnoremap <leader>p :set paste!<cr>
nnoremap <leader>] :%s/\s\+$//<cr>
nnoremap <leader>. :next<cr>
nnoremap <leader>, :prev<cr>
nnoremap <leader>> :wnext<cr>
nnoremap <leader>_ :set wrap!<cr>
nnoremap <Leader>l :list!<cr>
nnoremap <silent> vv <C-w>v

" Vimux config
" Prompt for a command to run
map <Leader>vp :VimuxPromptCommand<CR>
" Run last command executed by VimuxRunCommand
map <Leader>vl :VimuxRunLastCommand<CR>
" Inspect runner pane
map <Leader>vi :VimuxInspectRunner<CR>
" Zoom the tmux runner pane
map <Leader>vz :VimuxZoomRunner<CR>

" Nerdtree config

" Automatically open NerdTree on start
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
" Leader f to toggle
nnoremap <Leader>f :NERDTreeToggle<Enter>
" Leader v to jump to current file
nnoremap <silent> <Leader>v :NERDTreeFind<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
let NERDTreeShowHidden=1
let NERDTreeAutoDeleteBuffer = 1
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
let NERDTreeWinSize = 50

" Don't hide quotes in vim-json
let g:vim_json_syntax_conceal=0

" Ale config
let g:ale_lint_on_text_changed="Normal"
let g:ale_lint_on_insert_leave=1
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_sign_error = '✘'
let g:ale_sign_warning = '⚠'

autocmd FileType qf setlocal wrap

nnoremap [l :lnext<CR>
nnoremap ]l :lprev<CR>

" Syntax for multiple tag files are
" set tags=/my/dir1/tags, /my/dir2/tags
set tags=tags;$HOME/.vim/tags/

" TagList Plugin Configuration
let Tlist_GainFocus_On_ToggleOpen = 1
let Tlist_Close_On_Select = 1
let Tlist_Use_Right_Window = 1
let Tlist_File_Fold_Auto_Close = 1
map <F7> :TlistToggle<CR>

" Viewport Controls
" ie moving between split panes
" nnoremap <C-J> <C-W><C-J>
" nnoremap <C-K> <C-W><C-K>
" nnoremap <C-L> <C-W><C-L>
" nnoremap <C-H> <C-W><C-H>

set splitbelow
set splitright

" Add the virtualenv's site-packages to vim path
py3 << EOF
import os.path
import sys
if 'VIRTUAL_ENV' in os.environ:
    project_base_dir = os.environ['VIRTUAL_ENV']
    sys.path.insert(0, project_base_dir)
    activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
    compiled = compile(open(activate_this, "rb").read(), activate_this, 'exec')
    exec(compiled, dict(__file__=activate_this))
EOF

if &term =~ "xterm\\|rxvt"
  " use an orange cursor in insert mode
  let &t_SI = "\<Esc>]12;orange\x7"
  " use a red cursor otherwise
  let &t_EI = "\<Esc>]12;red\x7"
  silent !echo -ne "\033]12;red\007"
  " reset cursor when vim exits
  autocmd VimLeave * silent !echo -ne "\033]112\007"
  " use \003]12;gray\007 for gnome-terminal
endif

if &term =~ '^xterm'
  " solid underscore
  let &t_SI .= "\<Esc>[4 q"
  " solid block
  let &t_EI .= "\<Esc>[2 q"
  " 1 or 0 -> blinking block
  " 3 -> blinking underscore
  " Recent versions of xterm (282 or above) also support
  " 5 -> blinking vertical bar
  " 6 -> solid vertical bar
endif

" Security for gopass
au BufNewFile,BufRead /dev/shm/gopass.* setlocal noswapfile nobackup noundofile

" Terraform plugin
let g:terraform_align=1
let g:terraform_fold_sections=0
let g:terraform_fmt_on_save=1

let vim_markdown_preview_github=1
let vim_markdown_preview_hotkey='<C-m>'
let vim_markdown_preview_use_xdg_open=1

noremap <leader>x !%xmllint --format --recover -

" Make the autocomplete sane
set wildmode=longest,list,full
set wildmenu

" Black config
autocmd BufWritePre *.py execute ':Black'

" The Silver Searcher
if executable('ag')
  " Use ag over grep
  set grepprg=ag\ --nogroup\ --nocolor

  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'

  " ag is fast enough that CtrlP doesn't need to cache
  let g:ctrlp_use_caching = 0
endif

" bind K to grep word under cursor
nnoremap K :grep! "\b<C-R><C-W>\b"<CR>:cw<CR>

" bind \ (backward slash) to grep shortcut
command -nargs=+ -complete=file -bar Ag silent! grep! <args>|cwindow|redraw!
nnoremap \ :Ag<SPACE>

" store swap files in a common temp directory
set directory^=$HOME/.vim/tmp//

" Don't fold on startup
set foldlevelstart=20

" --- Filetype Specific Settings ---

" - Python

au FileType python setl textwidth=79 " PEP-8 Friendly
au FileType python setl foldmethod=indent
au FileType python setl foldlevel=99

" - JSonnet

au FileType jsonnet setl ts=2 sw=2 sts=2

" - YAML

autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
let g:indentLine_char = '⦙'

" ----------------------------------

set mouse=a
set ttymouse=sgr
set ttyfast

" Use system clipboard by default
set clipboard^=unnamed,unnamedplus

" ===============   Settings for CoC ========================
set updatetime=300
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
inoremap <silent><expr> <TAB>
    \ coc#pum#visible() ? coc#pum#next(1) :
    \ CheckBackspace() ? "\<Tab>" :
    \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

inoremap <silent><expr> <cr> coc#pum#visible() ? coc#_select_confirm() : "\<C-g>u\<CR>"

function! CheckBackspace() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-@> coc#refresh()
